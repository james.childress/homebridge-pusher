import { PlatformAccessory, PlatformConfig } from 'homebridge';

export interface PusherPlatformConfig extends PlatformConfig {
  name: string;
  subscriptions?: PusherAccessoryContext[];
  publishers?: PusherAccessoryContext[];
}

export interface PusherAccessoryContext {
  name: string;
  appId: string; // TODO: only needed by publishers
  key: string;
  secret: string; // TODO: only needed by publishers
  cluster: string;
  channel: string;
  event: string;
}

export type PusherAccessory = PlatformAccessory<PusherAccessoryContext>;
