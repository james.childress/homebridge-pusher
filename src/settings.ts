import { PusherAccessoryContext } from './types';

export const PLATFORM_NAME = 'Pusher';
export const PLUGIN_NAME = 'homebridge-pusher';

export const DEVICE_MANUFACTURER = 'Pusher Homebridge Plugin';
export const SUBSCRIBER_DEVICE_MODEL = 'Pusher Subscriber';
export const PUBLISHER_DEVICE_MODEL = 'Pusher Publisher';

/**
 * For publishers, the duration that a switch stays in the 'on' state.
 * For subscribers, the duration that sensor detects contact when an event is received.
 */
export const CONTACT_DURATION = 1000;

export const deviceSerialNumber = (context: PusherAccessoryContext) =>
  `${context.key}-${context.channel}-${context.event}`;
