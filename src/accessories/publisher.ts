import Pusher from 'pusher';

import { PusherHomebridgePlatform } from '../platform';
import { PusherAccessory } from '../types';
import { DEVICE_MANUFACTURER, PUBLISHER_DEVICE_MODEL, CONTACT_DURATION, deviceSerialNumber } from '../settings';

type PublisherSwitchState = { isOn: boolean };

export const pusherPublisherAccessory = (platform: PusherHomebridgePlatform, accessory: PusherAccessory) => {
  const { Characteristic, Service, log } = platform;
  const {
    displayName,
    context: { channel, event },
  } = accessory;

  const state: PublisherSwitchState = { isOn: false };
  let switchTimeout: NodeJS.Timeout | undefined;
  const pusher = new Pusher(accessory.context);

  const service = accessory.getService(Service.Switch) || accessory.addService(Service.Switch);

  accessory
    .getService(Service.AccessoryInformation)!
    .setCharacteristic(Characteristic.Manufacturer, DEVICE_MANUFACTURER)
    .setCharacteristic(Characteristic.Model, PUBLISHER_DEVICE_MODEL)
    .setCharacteristic(Characteristic.SerialNumber, deviceSerialNumber(accessory.context));

  service
    .setCharacteristic(Characteristic.Name, displayName)
    .getCharacteristic(Characteristic.On)
    .onGet(() => state.isOn)

    .onSet((isOn) => {
      // Cancel the timeout that will turn the switch off.
      switchTimeout && clearTimeout(switchTimeout);

      // If the switch is turned on, send the pusher event and then turn the switch off after the debounce duration.
      if (isOn) {
        state.isOn = true;
        pusher.trigger(channel, event, '').then(
          () => log.info(`'${displayName}' sent event '${event}' to channel '${channel}'`),
          (err) => log.error(`'${displayName}' failed to send event '${event}' to channel ${channel}:`, err)
        );

        // Turn the switch off after the duration.
        switchTimeout = setTimeout(() => {
          state.isOn = false;
          service.updateCharacteristic(Characteristic.On, false);
        }, CONTACT_DURATION);

        // If thie switch is turned off, only update our internal state to reflect the change.
      } else {
        state.isOn = false;
      }
    });
};
