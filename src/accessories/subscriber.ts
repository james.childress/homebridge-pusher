import { Service } from 'homebridge';
import Pusher from 'pusher-client';

import { PusherHomebridgePlatform } from '../platform';
import { PusherAccessory } from '../types';
import { DEVICE_MANUFACTURER, SUBSCRIBER_DEVICE_MODEL, CONTACT_DURATION, deviceSerialNumber } from '../settings';

type SubscriberSensorState = { isOpen: boolean };

/** Return the Homekit-compatible value of the sensor's state. */
const getContactState = ({ Characteristic }: PusherHomebridgePlatform, { isOpen }: SubscriberSensorState) =>
  isOpen ? Characteristic.ContactSensorState.CONTACT_NOT_DETECTED : Characteristic.ContactSensorState.CONTACT_DETECTED;

/** Update sensor state and notify HomeKit. */
const setContactState = (
  platform: PusherHomebridgePlatform,
  state: SubscriberSensorState,
  service: Service,
  isOpen: boolean
) => {
  state.isOpen = isOpen;
  service.updateCharacteristic(platform.Characteristic.ContactSensorState, getContactState(platform, state));
};

/** The sensor is active if the Pusher connection is connected. */
const getActiveState = (pusher: Pusher) => pusher.connection.state === 'connected';

export const pusherSubscriberAccessory = (platform: PusherHomebridgePlatform, accessory: PusherAccessory) => {
  const { Characteristic, Service, log } = platform;
  const {
    displayName,
    context: { key, cluster, channel, event },
  } = accessory;

  const state: SubscriberSensorState = { isOpen: false };
  let sensorTimeout: NodeJS.Timeout | undefined;
  const pusher = new Pusher(key, { cluster });

  const service = accessory.getService(Service.ContactSensor) || accessory.addService(Service.ContactSensor);

  accessory
    .getService(Service.AccessoryInformation)!
    .setCharacteristic(Characteristic.Manufacturer, DEVICE_MANUFACTURER)
    .setCharacteristic(Characteristic.Model, SUBSCRIBER_DEVICE_MODEL)
    .setCharacteristic(Characteristic.SerialNumber, deviceSerialNumber(accessory.context));

  service.setCharacteristic(Characteristic.Name, displayName);

  const _getContactState = getContactState.bind(this, platform, state);
  const _setContactState = setContactState.bind(this, platform, state, service);
  const _getActiveState = getActiveState.bind(this, pusher);

  /** Report whether the sensor is detecting contact. */
  service.getCharacteristic(Characteristic.ContactSensorState).onGet(_getContactState);

  /** The sensor is active if the Pusher connection is connected. */
  service.getCharacteristic(Characteristic.StatusActive).onGet(_getActiveState);

  /**
   * Detect contact when Pusher receives the subscribed event.
   * After the debounced duration, stop detecting contact.
   */
  pusher.subscribe(channel).bind(event, () => {
    log.info(`'${displayName}' received event '${event}' on channel '${channel}'`);
    /** Cancel the timeout that will end the sensor detection. */
    sensorTimeout && clearTimeout(sensorTimeout);
    _setContactState(true);
    sensorTimeout = setTimeout(() => _setContactState(false), CONTACT_DURATION);
  });
};
