import {
  API,
  DynamicPlatformPlugin,
  Logger,
  PlatformAccessory,
  Service,
  Characteristic,
  PlatformConfig,
} from 'homebridge';

import { deviceSerialNumber, PLATFORM_NAME, PLUGIN_NAME } from './settings';
import { pusherSubscriberAccessory } from './accessories/subscriber';
import { pusherPublisherAccessory } from './accessories/publisher';
import { PusherAccessory, PusherAccessoryContext } from './types';

export class PusherHomebridgePlatform implements DynamicPlatformPlugin {
  public readonly Service: typeof Service = this.api.hap.Service;
  public readonly Characteristic: typeof Characteristic = this.api.hap.Characteristic;

  public readonly accessories: PlatformAccessory[] = [];

  constructor(public readonly log: Logger, public readonly config: PlatformConfig, public readonly api: API) {
    this.api.on('didFinishLaunching', () => {
      (this.config.subscriptions || []).forEach(this.registerAccessory.bind(this, pusherSubscriberAccessory));
      (this.config.publishers || []).forEach(this.registerAccessory.bind(this, pusherPublisherAccessory));
    });
  }

  configureAccessory(accessory: PlatformAccessory) {
    this.log.info('Loading accessory from cache:', accessory.displayName);
    this.accessories.push(accessory);
  }

  registerAccessory(
    accessoryHandler: (platform: PusherHomebridgePlatform, accessory: PusherAccessory) => void,
    context: PusherAccessoryContext
  ) {
    const uuid = this.api.hap.uuid.generate(deviceSerialNumber(context));

    const cachedAccessory = this.accessories.find((accessory) => accessory.UUID === uuid) as PusherAccessory;

    if (cachedAccessory) {
      this.log.info('Restoring existing accessory from cache:', cachedAccessory.displayName);
      accessoryHandler(this, cachedAccessory);
    } else {
      this.log.info('Adding new accessory:', context.name);
      const accessory = new this.api.platformAccessory<PusherAccessoryContext>(context.name, uuid);
      accessory.context = context;
      accessoryHandler(this, accessory);
      this.api.registerPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, [accessory]);
    }
  }
}
