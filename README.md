# Pusher plugin for Homebridge

This platform plug-in for [Homebridge](https://homebridge.io/) allows you to add two types of devices to HomeKit: subscribers and publishers.

### Subcribers

Subscribers are represented as [contact sensors](https://developers.homebridge.io/#/service/ContactSensor). They [subscribe](https://pusher.com/docs/channels/using_channels/public-channels/#subscribe) to a single event on a Pusher channel. By default, the sensor remains in the 'closed' state. When the specified event is received, the sensor switches to the 'open' state for one second.

This allows you to trigger HomeKit automations from any web app or automation platform, such as [Zapier](https://zapier.com), that has a [Pusher integration](https://zapier.com/apps/pusher/integrations).

Note: I couldn't get Zapier's existing Pusher integration to work, so I [wrote my own](https://gitlab.com/james.childress/zapier-pusher-integration).

### Publishers

Publishers are represented as [switches](https://developers.homebridge.io/#/service/Switch). When switched on, they [publish](https://pusher.com/docs/channels/server_api/http-api/#publishing-events) a single event to a Pusher channel and then return to the off state. This allows you to trigger automations on any web app or automation platform that subscribes to Pusher events.

It also allows you to chain automations between multiple HomeKit homes by having a publisher at one home and a subscriber at another.

# Future Features

- Support for [private](https://pusher.com/docs/channels/using_channels/private-channels/) channels. Currently, only public channels are supported.

- Ability to subscribe/publish multiple events from a single subscriber/publisher.

- Ability to act on [event data](https://pusher.com/docs/channels/using_channels/events/#data-1956775764) instead of just channel and event names.
